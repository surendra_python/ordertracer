# Import Module
import ftplib 
from configparser import ConfigParser
import os

from debugpy import connect


class Remote:
    def __init__(self):
        path_current_directory = os.path.dirname(__file__)
        file = os.path.join(path_current_directory, 'configuration', "config.ini")
        config = ConfigParser()
        config.read(file)
        self.hostname = config.get('Remote' , 'hostname')
        self.userId = config.get('Remote' , 'userId')
        self.pwd = config.get('Remote' , 'pwd')
        self.fix_filename = config.get('Remote' , 'fix_filename')
        
    def Connect(self):
        self.ftp_server = ftplib.FTP(self.hostname, self.userId, self.pwd)

    def getfilehandle(self):
        with open(self.fix_filename, "wb") as file:
            self.ftp_server.retrbinary(f"RETR {self.fix_filename}", file.write) 
            self.ftp_server.quit()
        return self.fix_filename


remote = Remote()
remote.Connect()
file = remote.getfilehandle()
#fixlog = open(file, "r")
