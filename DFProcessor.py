import pandas as pd
from FIXLogProcessor import*
import os


class DFProcessor:
    def __init__(self):
        self.df_extract = fixlogprocessor.df_extract


    def find_all_tags_in_fix_log(self):
        df_extract_tag = df_extract.tag.unique()
        print(df_extract_tag)
        tag_list = df_extract_tag.tolist()
        return tag_list

    def transpose_FixDF(self):
        #self.line_to_df()
        df = pd.DataFrame(columns = tag_list) 
        msg_no_start = df_extract.iloc[0]["Msg_No"]
        Last_Index = df_extract.index[-1]
        line = "" 
        print("---->",msg_no_start)
        for ind in df_extract.index:
            msg_no = df_extract["Msg_No"][ind]
            if(msg_no == msg_no_start):  
                print(msg_no)
                line = line + df_extract["tag"][ind] + "=" +  df_extract["value"][ind] + "|"
                print(line) 
                if(ind == Last_Index):
                    line = line.strip("|")
                    df = self.line_to_df(line,df)
                    print(df.head())                
            else:
                line = line.strip("|")
                df = self.line_to_df(line,df)
                print(df.head())
                msg_no_start = msg_no 
                line = ""
                line = line + df_extract["tag"][ind] + "=" +  df_extract["value"][ind] + "|"        
        return df

    def line_to_df(self, line, df):
        #self.find_value_from_tag()
        size = df.size
        linedict = {}
        for ind in df.columns.values.tolist():
            tag = ind
            tagvalue = line.split("|")
            value = self.find_value_from_tag(tagvalue, tag)
            if(value != None):
                linedict[tag] =  value
            else:
                linedict[tag] = ""
        df_dictionary = pd.DataFrame([linedict])
        df = pd.concat([df, df_dictionary], ignore_index=True)      
        return df

    def find_value_from_tag(self,tagvalue, tag):
        for line in tagvalue:
            splitarray = line.split("=")
            tag1 = splitarray[0]
            value1 = splitarray[1]
            print('value:',value1)
            if(tag == tag1):
                return value1
        return ""

dfprocessor = DFProcessor()
tag_list = dfprocessor.find_all_tags_in_fix_log()
#line_to_df = dfprocessor.line_to_df()
#find_value_from_tag = dfprocessor.find_value_from_tag()
df_extract_t = dfprocessor.transpose_FixDF()


print(df_extract_t.columns)
df_extract_t.index.names = ['Sr.No']
print(df_extract_t.columns)
print(df_extract_t.head())
df_extract_t.to_csv('FIXLogNew1.csv',index=True)
