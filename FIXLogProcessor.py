from FIXLog_Remote import*
import os
import pandas as pd

class FIXLogProcessor:
    def __init__(self):
        path_current_directory = os.path.dirname(__file__)
        file = os.path.join(path_current_directory, 'configuration', "config.ini")
        config = ConfigParser()
        config.read(file)
        self.text = config.get('FIXLogProcessor' , 'text')
        self.fix_filename = config.get('Remote' , 'fix_filename')

    def Filter(self):
        os.path.exists(self.fix_filename)
        try:
            fixlog = open(self.fix_filename, "r")
            lines = fixlog.readlines()
            new_list = []
            idx = 0
            for line in lines:
                if self.text in line:
                    new_list.insert(idx, line)
                    idx += 1
            fixlog.close()
            if len(new_list)==0:
                print("\n\"" +self.text+ "\" is not found in \"" +self.fix_filename+ "\"!")
            else:
                lineLen = len(new_list)
                print("\n**** Lines containing \"" +self.text+ "\" ****\n")
                for i in range(lineLen):
                    print(end=new_list[i])
                print()
            return new_list    
        except :
            print("\nThe file doesn't exist!")

    def fix_linearray_to_hash(self):
        fix_linehash = {}
        count = 0
        seq = []
        tag = []
        value = []
        fix_linehash["Msg_No"] = seq
        fix_linehash["tag"] = tag
        fix_linehash["value"] = value
        for line in fix_linearray:
            line = line.strip()
            taglist = line.split("|")
            taglist = taglist[0:len(taglist)-1]
            for tagvalue in taglist:
                tag = tagvalue.split("=")[0]
                value = tagvalue.split("=")[1]
                seq = fix_linehash["Msg_No"]
                seq.append(count)
                fix_linehash["Msg_No"] = seq
                tagarray = fix_linehash["tag"]
                tagarray.append(tag)
                fix_linehash["tag"] = tagarray
                valuearray = fix_linehash["value"]
                valuearray.append(value)
                fix_linehash["value"] = valuearray
            count = count + 1        
        return fix_linehash

    def df_extract(self):
        df_extract = pd.DataFrame(fix_linehash)
        return df_extract

fixlogprocessor = FIXLogProcessor()
fix_linearray = fixlogprocessor.Filter()
fix_linehash = fixlogprocessor.fix_linearray_to_hash()
df_extract = fixlogprocessor.df_extract()
print(df_extract.head(100))